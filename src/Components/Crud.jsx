import React,{useState,useEffect} from 'react'
import axios from 'axios'
import nextId from "react-id-generator";
import ReadCrud from './ReadCrud';
const Crud = () => {
  //
    const [posts,setPosts]=useState([])
    const [addPost,setaddpost]=useState({
        userId:5,
        title:'',
        body:''
    })


    //Get ID 
    const [editPostId,setEditPostId]=useState(null)
    const [editFromdata,setEditFormdata]=useState({
        userId:5,
        title:'',
        body:''
    })



      //Get Form Values
  const handleChange = (input) => (e) => {
    e.preventDefault()
    console.log(addPost);
    setaddpost({ ...addPost, [input]: e.target.value });
  }


     //Add Data To Table
   const handleAddpost = (e)=>{
       e.preventDefault()
       const newPost ={
          id: nextId(),
           userId:addPost.userId,
           title:addPost.title,
           body:addPost.body
       }
       const newPosts =[...posts,newPost]
       setPosts(newPosts)
   }


   //Edit data value
   const handledEditChange=(input)=>(e)=>{
       e.preventDefault()
       setEditFormdata({...editFromdata,[input]:e.target.value})
   }
   
   const handleEditPostFrom =(e,post)=>{
       e.preventDefault()
       setEditPostId(post.id)
       const formValues ={
           userId:post.userId,
           title:post.title,
           body:post.body
}
setEditFormdata(formValues)
   }
    //Save Form Data
  const handleFormSave = (e) => {
    e.preventDefault()
    const savePost = {
      id: editPostId,
      userId: editFromdata.userId,
      title: editFromdata.title,
     body:editFromdata.body
      
    }
    const newPosts = [...posts]
    const formIndex = posts.findIndex((post) => post.id === editPostId);
    newPosts[formIndex] = savePost
    setPosts(newPosts)
    setEditPostId(null)
    console.log(editPostId);
  }

//Delete
  const handleDelete = (e) => {
    e.preventDefault()
    const newPosts = [...posts]
    const formIndex = posts.findIndex((post) => post.id === editPostId);
    newPosts.splice(formIndex, 1);
    setPosts(newPosts)
  }


    // get data  json placeholder
    useEffect(()=>{
      axios.request({
          method:'GET',
          url:"https://jsonplaceholder.typicode.com/posts/",
          params:{
              _limit:5
          }
      }).then(res=>{
          setPosts(res.data)
      })
    },[])
    console.log(posts)


  return (
    <div>
   
        <table className='table table-dark table-striped'>
            <thead>
                <tr>
                    <th scope='col'>user Id</th>
                    <th scope='col'>ID</th>
                    <th scope='col'>Title</th>
                    <th scope='col'>Body</th>
                    <th scope='col'>Action</th>
                    <th scope='col'>Action</th>
                </tr>
            </thead>
            <tbody>
            <ReadCrud posts={posts}
                handleEditPostFrom={handleEditPostFrom}
            />
            </tbody>
        </table>
          {/*ADD Modal*/}
        <div className="modal fade" id="addModalForm" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{backgroundColor:"#303030",color:"whitesmoke"}}>
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">Add New Post</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handleAddpost}>
                <div className="mb-3">
                  <label className="form-label">User ID</label>
                  <input
                    type="text"
                    className="form-control"
                    name="userId"
                    resource=''
                    disabled
                    onChange={handleChange("userId")}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Title</label>
                  <input
                    type="text"
                    className="form-control"
                    name="title"
                    placeholder="title"
                    required
                    onChange={handleChange("title")}
                    
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Body</label>
                  <textarea
                    rows="4"
                    cols="50"
                    className="form-control"
                    name="body"
                    placeholder="body"
                    required
                    onChange={handleChange("body")}
                    
                  ></textarea>
                </div>
                <div className="modal-footer d-block">
                  <button type="submit" data-bs-dismiss="modal" className="btn btn-outline-info">Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      {/*EDIT  Modal*/}
      <div className="modal fade" id="editModalForm" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content" style={{backgroundColor:"#303030",color:"whitesmoke"}}>

            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">EDIT</h5>
              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div className="modal-body">
              <form onSubmit={handleFormSave}>
                <div className="mb-3">
                  <label className="form-label">User ID</label>
                  <input
                    type="text"
                    className="form-control"
                    name="userId"
                    required
                    disabled
                    onChange={handledEditChange("userId")}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Title</label>
                  <input
                    type="text"
                    className="form-control"
                    name="title"
                    required
                    onChange={handledEditChange("title")}
                  />
                </div>
                <div className="mb-3">
                  <label className="form-label">Body</label>
                  <textarea
                    rows="4"
                    cols="50"
                    className="form-control"
                    name="body"
                    onChange={handledEditChange("body")}
                    required
                    
                  ></textarea>
                </div>
                <div className="modal-footer d-block">
                  <button
                   className="btn btn-outline-info"
                    type="submit"
                    data-bs-dismiss="modal"
                   
                  >Save </button>
                  <button onClick={handleDelete}
                    className="btn btn-outline-info"
                    type="submit"
                    data-bs-dismiss="modal"
                  >Delete </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  )
}

export default Crud
