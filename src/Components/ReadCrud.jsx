import React from 'react'

const ReadCrud = ({posts,handleEditPostFrom}) => {
  return (
    <>
    {posts.map((post)=>
           <tr>
               <td>{post.userId}</td>
               <td>{post.id}</td>
               <td>{post.title}</td>
               <td>{post.body}</td>
               <td>
               <button type="button" class="btn btn-outline-info"  data-bs-toggle="modal" data-bs-target="#editModalForm"  onClick={(e)=>handleEditPostFrom(e,post)}>EDIT</button>
               </td>
               <td>
               <button type="button" class="btn btn-outline-info" data-bs-toggle="modal" data-bs-target="#addModalForm"  onClick={(e)=>handleEditPostFrom(e,post)}>ADD</button>
               </td>   
 </tr>)}
</>
  )
}

export default ReadCrud